package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote._
import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql}
import org.scalatest.{AsyncFunSuite, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random

class RestFriendsVerticleTest extends AsyncFunSuite with BeforeAndAfterEach {

  private final val RANDOM_RANGE=10000
  private final val DURATION=4

  final val DefaultDuration: Duration = Duration(DURATION, TimeUnit.SECONDS)
  final val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString
  val vertx: Vertx = Vertx.vertx
  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(vertx.getOrCreateContext())
  var verticle: RestVerticle = _
  var deploymentID: String = _

  override def beforeEach(): Unit = {
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    while (PlayerSql.playerExist(DefaultFriend)) DefaultFriend = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    PlayerSql.addPlayer(DefaultFriend, DefaultHash)
    FriendSql.askForFriendship(DefaultFriend, DefaultUsername)
    FriendSql.acceptFriendShip(DefaultUsername, DefaultFriend)
  }

  test("Post to friend route should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, FriendsRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Put to friend route should not be permitted") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, FriendsRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("delete to friend route should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, FriendsRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Get to friend route with invalid credentials should return Unauthorized") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, FriendsRoute)
      .addQueryParam(ApiUsername, "Non" + DefaultUsername)
      .addQueryParam(ApiPwHash, "Non" + DefaultHash)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == StatusCodes.Unauthorized.intValue))
  }
  test("Get to friend route with valid credentials should return the friends list") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, FriendsRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => {
        assert(response.bodyAsJsonArray().get.getList.contains(DefaultFriend))
        response.statusCode()
      })
      .map(status => assert(status == StatusCodes.OK.intValue))
  }

  override def afterEach(): Unit = {
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultFriend)
  }
}

