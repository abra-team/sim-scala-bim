package it.unibo.ssb.model

import it.unibo.ssb.model.remote.sql.executor.SqlManager
import org.scalatest.FunSuite

class DatabaseConnectionTest extends FunSuite {

  test("SqlManager have to access and execute correctly queries") {
    SqlManager.executeQuery("DELETE FROM test WHERE prova=1")
    SqlManager.executeQuery("INSERT INTO test (prova) VALUES (1);")
    val result = SqlManager.selectQuery("SELECT prova FROM test WHERE prova=1")
    val resultset = result._1.get
    resultset.next()
    assert(resultset.getInt("prova") == 1)
  }
}

