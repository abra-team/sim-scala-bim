package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.OK
import io.vertx.core.json.JsonObject
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote.sql.executor.{PlayerSql, SqlManager}
import it.unibo.ssb.model.remote.{ApiPwHash, ApiScore, ApiUsername, DefaultApiPort, DefaultHostname, HallOfFameRoute}
import org.scalatest.{AsyncFunSuite, BeforeAndAfterAll}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random

class HallOfFameApiTest extends AsyncFunSuite with BeforeAndAfterAll {

  private final val RANDOM_RANGE=10000
  private final val DURATION=10
  private final val SCORE_1=900
  private final val SCORE_2=1100

  final val DefaultDuration: Duration = Duration(DURATION, TimeUnit.SECONDS)
  final val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString
  val vertx: Vertx = Vertx.vertx
  val DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(vertx.getOrCreateContext())
  var verticle: RestVerticle = _
  var deploymentID: String = _

  override def beforeAll(): Unit = {
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    PlayerSql.addPlayer(DefaultFriend, DefaultHash)
    PlayerSql.updateScore(DefaultUsername, SCORE_1)
    PlayerSql.updateScore(DefaultFriend, SCORE_2)
  }

  test("Post to HallOfFameRoute should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, HallOfFameRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Put to HallOfFameRoute should not be permitted") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, HallOfFameRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("delete to HallOfFameRoute should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, HallOfFameRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Get to HallOfFameRoute with valid credentials should return the friends list") {
    var players: List[(String, Int)] = List()
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, HallOfFameRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => {
        response.bodyAsJsonArray().get.forEach {
          case a: JsonObject =>
            val name: String = a.getString(ApiUsername)
            val score: Int = a.getInteger(ApiScore)
            players = players ++: List(Tuple2(name, score))
          case _ => fail("NON RIESCE A CASTARE IL JSON")
        }
        assert(
          response.statusCode() == OK.intValue)
      })
  }

  override def afterAll(): Unit = {
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultFriend)
  }
}