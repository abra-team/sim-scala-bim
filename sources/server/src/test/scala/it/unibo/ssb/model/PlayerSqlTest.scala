package it.unibo.ssb.model

import it.unibo.ssb.model.remote.sql.executor.PlayerSql
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.util.Random

class PlayerSqlTest extends FunSuite with BeforeAndAfterAll {

  private final val RANDOM_RANGE: Int = 10000
  private final val DEFAULT_SLEEP: Int = 500
  private final val SCORE = 10

  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString

  override def beforeAll(): Unit = {
    Thread.sleep(DEFAULT_SLEEP)
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)

  }

  test("a player added must have 1000 point") {
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    Thread.sleep(DEFAULT_SLEEP)
    assert(PlayerSql.getScore(DefaultUsername) == 1000)
  }
  test("a player must login with its credential") {
    assert(PlayerSql.isCredentialOK(DefaultUsername, DefaultHash))
  }
  test("a player added must exist") {
    assert(PlayerSql.playerExist(DefaultUsername))
    assert(!PlayerSql.playerNotExist(DefaultUsername))
  }
  test("a player can increment its score") {
    PlayerSql.updateScore(DefaultUsername, SCORE)
    Thread.sleep(DEFAULT_SLEEP)
    assert(PlayerSql.getScore(DefaultUsername) == SCORE)
  }
  test("a player removed must not exist") {
    PlayerSql.removePlayer(DefaultUsername)
    assert(PlayerSql.playerNotExist(DefaultUsername))
  }

  override def afterAll(): Unit = {
    PlayerSql.removePlayer(DefaultUsername)
  }
}
