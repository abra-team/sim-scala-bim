package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes._
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql}
import it.unibo.ssb.model.remote.{ApiFriend, ApiPwHash, ApiUsername, DefaultApiPort, DefaultHostname, RequestFriendshipRoute}
import org.scalatest.{AsyncFunSuite, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.Random

class RequestFriendshipApiTest extends AsyncFunSuite with BeforeAndAfterEach {

  private final val RANDOM_RANGE=10000
  private final val DURATION=10

  final val DefaultDuration: FiniteDuration = Duration(DURATION, TimeUnit.SECONDS)
  final val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString
  val vertx: Vertx = Vertx.vertx
  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(
    vertx.getOrCreateContext()
  )
  var verticle: RestVerticle = _
  var deploymentID: String = _


  override def beforeEach(): Unit = {
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    while (PlayerSql.playerExist(DefaultFriend)) DefaultFriend = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultFriend, DefaultHash)
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)
  }

  test("Get to RequestFriendshipRoute route should not be permitted") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, RequestFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == BadRequest.intValue))
  }

  test("Post to RequestFriendshipRoute route should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, RequestFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == BadRequest.intValue))
  }

  test("delete to RequestFriendshipRoute route should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, RequestFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == BadRequest.intValue))
  }

  test("Put to RequestFriendshipRoute route with invalid credentials should return Unauthorized") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, RequestFriendshipRoute)
      .addQueryParam(ApiUsername, "username" + DefaultUsername)
      .addQueryParam(ApiPwHash, "hash" + DefaultHash)
      .addQueryParam(ApiFriend, "friend" + DefaultFriend)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == Unauthorized.intValue))
  }
  test("Put to RequestFriendshipRoute route with valid credentials should return Ok") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, RequestFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => {
        assert(
          response.statusCode() == OK.intValue && FriendSql.friendRequests(DefaultFriend).contains(DefaultUsername))
      })
  }

  override def afterEach(): Unit = {
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultFriend)

  }
}
