package it.unibo.ssb.model

import it.unibo.ssb.model.remote.sql.executor.PlayerSql
import it.unibo.ssb.model.remote.sql.executor.PlayerSql.playerExist
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.collection.mutable.ListBuffer
import scala.util.Random

class LeaderBoardTest extends FunSuite with BeforeAndAfterAll {

  private final val RANDOM_RANGE: Int = 10000

  val winners: ListBuffer[(String, String)] = ListBuffer()
  val losers: ListBuffer[(String, String)] = ListBuffer()


  override def beforeAll(): Unit = {
    for (_ <- 0 to 3) {
      var player = (Random.nextInt(RANDOM_RANGE).toString, Random.nextInt(RANDOM_RANGE).toString)
      while (playerExist(player._1)) player = (Random.nextInt(RANDOM_RANGE).toString, Random.nextInt(RANDOM_RANGE).toString)
      PlayerSql.addPlayer(player._1, player._2)
      winners.+=(player)
    }
    for (_ <- 0 to 3) {
      var player = (Random.nextInt(RANDOM_RANGE).toString, Random.nextInt(RANDOM_RANGE).toString)
      while (playerExist(player._1)) player = (Random.nextInt(RANDOM_RANGE).toString, Random.nextInt(RANDOM_RANGE).toString)
      PlayerSql.addPlayer(player._1, player._2)
      losers.+=(player)
    }

    super.beforeAll()
  }

  override protected def afterAll(): Unit = {
    (winners ++ losers).foreach(p => PlayerSql.removePlayer(p._1))
    super.afterAll()
  }

  test("Leader board must load correctly players from db") {
    (winners ++ losers).foreach(p => {
      assert(LeaderBoard().getScoreOf(p._1) != 0)
    })
  }

  test("Winners must have grater score than loser after update") {

    // check all player have same score
    assert(
      (winners ++ losers).map(p => LeaderBoard().getScoreOf(p._1))
        .count(p => LeaderBoard().getScoreOf(winners.head._1)
          .equals(p))
        .equals((winners ++ losers).size)
    )

    LeaderBoard().update(winners.map(_._1).toList, losers.map(_._1).toList)

    // now i check winner have a greater score than loser
    winners.foreach(w => {
      losers.foreach(l => assert(LeaderBoard().getScoreOf(w._1) > LeaderBoard().getScoreOf(l._1)))
    })
  }

}
