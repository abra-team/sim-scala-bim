package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote._
import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql}
import org.scalatest.{AsyncFunSuite, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.Random

class FriendsRequestVerticleTest extends AsyncFunSuite with BeforeAndAfterEach {
  val vertx: Vertx = Vertx.vertx

  private final val RANDOM_RANGE=10000
  private final val DURATION=10

  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  final val DefaultDuration: FiniteDuration = Duration(DURATION, TimeUnit.SECONDS)
  final val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(vertx.getOrCreateContext())
  var verticle: RestVerticle = _
  var deploymentID: String = ""

  override def beforeEach():Unit= {
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    while (PlayerSql.playerExist(DefaultFriend)) DefaultFriend = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultFriend, DefaultHash)
    FriendSql.askForFriendship(DefaultFriend, DefaultUsername)
  }

  test("Post to friendRequest route should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, FriendsRequestRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Put to friendRequest route should not be permitted") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, FriendsRequestRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("delete to friendRequest route should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, FriendsRequestRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Get to friendRequest route with invalid credentials should return Unauthorized") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, FriendsRequestRoute)
      .addQueryParam(ApiUsername, "Non" + DefaultUsername)
      .addQueryParam(ApiPwHash, "Non" + DefaultHash)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == StatusCodes.Unauthorized.intValue))
  }
  test("Get to friendRequest route with valid credentials should return the friendsRequest list") {
    FriendSql.askForFriendship(DefaultFriend, DefaultUsername)
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, FriendsRequestRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => {
        assert(
          response.bodyAsJsonArray().get.getList.contains(DefaultFriend)
            && response.statusCode() == StatusCodes.OK.intValue)
      })
  }

  override def afterEach():Unit={
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultFriend)
    PlayerSql.removePlayer(DefaultUsername)
  }
}

