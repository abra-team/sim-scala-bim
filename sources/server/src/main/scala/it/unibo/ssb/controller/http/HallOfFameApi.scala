package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiUsername, HallOfFameRoute, ApiScore}

case class HallOfFameApi()  extends HttpApi(HttpMethod.GET, HallOfFameRoute) {

  override def handle(rc: RoutingContext): Unit = {
    var players: List[(String,Int)] = List()
    players = players ++ cache.hallOFFame
    val jsonArray = Json.emptyArr()
    players.foreach(f => jsonArray.add(Json.obj((ApiUsername,f._1),(ApiScore,f._2))))
    rc.response().setStatusCode(StatusCodes.OK.intValue).end(jsonArray.encodePrettily())
  }
}
