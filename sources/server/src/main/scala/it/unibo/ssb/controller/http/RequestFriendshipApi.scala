package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiFriend, ApiPwHash, ApiUsername, RequestFriendshipRoute}


case class RequestFriendshipApi() extends HttpApi(HttpMethod.PUT, RequestFriendshipRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    val f = rc.queryParams().get(ApiFriend)

    val sc = (u, p, f) match {
      case (Some(user), Some(pw), Some(friend)) if user.nonEmpty && pw.nonEmpty =>

        if (!cache.isCredentialOK(user, pw)) StatusCodes.Unauthorized
        else if (cache.friends(friend).contains(friend) || cache.friendshipsNotAccepted(friend).contains(friend)) {
          StatusCodes.AlreadyReported
        }
        else {
          cache.askForFriendship(user, friend)
          StatusCodes.OK
        }
      case _ => StatusCodes.BadRequest
    }
    rc.response().setStatusCode(sc.intValue).end(sc.defaultMessage)
  }
}
