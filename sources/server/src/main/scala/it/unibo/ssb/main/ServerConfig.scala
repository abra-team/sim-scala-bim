package it.unibo.ssb.main
import java.net.URL

import akka.actor.Address
import better.files.Resource
import com.typesafe.config.{ConfigException, Config => AkkaConfig, ConfigFactory => AkkaConfigFactory}
import it.unibo.ssb.model.remote
import it.unibo.ssb.model.remote._

/**
  * This class models a configuration for the server execution.
  */
case class ServerConfig(
    hostname: String = DefaultHostNameServer,
    port: Int = DefaultApiPort,
    akkaPort: Int = ServerConfig.DefaultAkkaPort,
    akkaHostname: String = ServerConfig.DefaultAkkaHostname
){
  /** The name of the server actorSystem. */
  val systemName: String = remote.AkkaSystemName

  /** The bind-hostname value of the server. */
  val boundHostname: String = remote.DefaultIpHostname

  /** The bind-hostname value of the server. */
  val boundPort: Int = akkaPort

  /** The base address of the server actorSystem. */
  val address: Address = Address("akka.tcp", systemName, hostname, akkaPort)
}

object ServerConfig{

  /** Base Akka key for remote TCP configurations. */
  private final val AkkaRemoteNettyTcp: String = "akka.remote.netty.tcp"
 
  /** Akka key for hostname. */
  final val AkkaLogicalHostname: String = s"$AkkaRemoteNettyTcp.hostname"
 
  /** Akka key for port. */
  final val AkkaLogicalPort: String = s"$AkkaRemoteNettyTcp.port"
 
  /** Akka key for bounded hostname. */
  final val AkkaBindHostname: String = s"$AkkaRemoteNettyTcp.bind-hostname"
 
  /** Akka key for bounded port. */
  final val AkkaBindPort: String = s"$AkkaRemoteNettyTcp.bind-port"

  /** Akka configuration file name. */
  private final val fileName: String = "application.conf"

  /** Akka possible configuration file.
    *
    * It may be non-present (ie. in test environments).
    */
  private final val confFile: URL = Resource.getUrl(fileName)

  /**
    * Local Akka [[akka.actor.ActorSystem ActorSystem]] basic configuration.
    *
    * @see [[https://doc.akka.io/docs/akka/2.5/general/configuration.html]]
    */
  val akkaConfig: AkkaConfig = AkkaConfigFactory.parseURL(confFile)

  /** Parse default Akka hostname from configuration file, if it defines it. */
  private val DefaultAkkaHostname: String = try {
    akkaConfig.getString(ServerConfig.AkkaLogicalHostname)
  } catch {
    case _: ConfigException.Missing => remote.DefaultIpHostname
    case e: Throwable => throw e
  }

  /** Parse default Akka port from configuration file, if it defines it. */
  private val DefaultAkkaPort: Int = try {
    akkaConfig.getString(ServerConfig.AkkaLogicalPort).toInt
  } catch {
    case _: ConfigException.Missing => remote.DefaultServerAkkaPort
    case e: Throwable => throw e
  }

}