package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiScore, ApiUsername, RankingRoute}

case class RankingApi() extends HttpApi(HttpMethod.GET, RankingRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    var position: Int = 0
    val sc = (u, p) match {
      case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
        if (cache.isCredentialOK(user, pw)) {
          position = cache.getRankingPosition(user)
          StatusCodes.OK
        } else StatusCodes.Unauthorized
      case _ => StatusCodes.BadRequest
    }
    val json = Json.obj((ApiScore, position))

    if (sc.equals(StatusCodes.OK)) rc.response().setStatusCode(sc.intValue).end(json.encodePrettily())
    else rc.response().setStatusCode(sc.intValue).end()
  }
}
