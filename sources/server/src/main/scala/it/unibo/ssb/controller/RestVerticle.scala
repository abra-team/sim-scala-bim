package it.unibo.ssb.controller

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.ext.web.Router
import it.unibo.ssb.model.remote.{DefaultApiPort, DefaultHostname}
import it.unibo.ssb.controller.http._

import scala.concurrent.Future

class RestVerticle(hostname: String = DefaultHostname, port: Int = DefaultApiPort) extends ScalaVerticle {

  override def startFuture(): Future[_] = {
    val router = Router.router(vertx)
    setRouteFor(router, AuthApi())
    setRouteFor(router, FriendsApi())
    setRouteFor(router, RegistrationApi())
    setRouteFor(router, FriendsRequestApi())
    setRouteFor(router, AcceptFriendshipApi())
    setRouteFor(router, ScoreApi())
    setRouteFor(router, RankingApi())
    setRouteFor(router, DiscoveryActorRefApi())
    setRouteFor(router, RequestFriendshipApi())
    setRouteFor(router, PlayersListApi())
    setRouteFor(router, RankingApi())
    setRouteFor(router, HallOfFameApi())
    setRouteFor(router, FriendsCandidateList())

    vertx
      .createHttpServer()
      .requestHandler(h => router.accept(h))
      .listenFuture(port, hostname)
      .map(_ => ())
  }

  private def setRouteFor(router: Router, httpApiVerticle: HttpApi): Unit = {
    badHttpMethodHandlersSetter(router, httpApiVerticle.method, httpApiVerticle.route)
    router.route(httpApiVerticle.method, httpApiVerticle.route).handler(httpApiVerticle)
  }

  private def badHttpMethodHandlersSetter(router: Router, goodMethod: HttpMethod, route: String): Unit = {
    HttpMethod.values().filter(!goodMethod.equals(_)).foreach(m => {
      router.route(m, route).handler(rc => {
        val sc = StatusCodes.BadRequest
        val msg = sc.intValue + ": " + sc.defaultMessage + ": " + sc.defaultMessage
        rc.response()
          .setStatusCode(sc.intValue)
          .end(msg)
      })
    })
  }
}
