package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiUsername, FriendsRequestRoute}
/**
** this class handles the API call to get the request of friendship that other players send.
* Only accept the GET method with the following parameters:
* *
* -username [[String]] of the player who wants to accept the friendship
* -password [[String]] of the player who wants to accept the friend request encrypted with the SHA256 algorithm
* *
* Return:
* -401 if the username and password do not match any player in the system
* -400 if one of the three parameters is missing
* -200 if the operation was successful,  return also a [[io.vertx.core.json.JsonArray]] contained the list of player that ask for friendship
* */
case class FriendsRequestApi() extends HttpApi(HttpMethod.GET, FriendsRequestRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    var friendsRequest: List[String] = List()
    val sc = (u, p) match {
      case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
        if (cache.isCredentialOK(user, pw)) {
          friendsRequest = friendsRequest ++ cache.friendRequests(user)
          StatusCodes.OK
        } else StatusCodes.Unauthorized
      case _ => StatusCodes.BadRequest
    }
    val jsonArray = Json.emptyArr()
    friendsRequest.foreach(f => jsonArray.add(f))
    rc.response().setStatusCode(sc.intValue).end(jsonArray.encodePrettily())
  }
}
