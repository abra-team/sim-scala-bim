package it.unibo.ssb.view.style

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.em
import java.awt.Font

class AuthenticationStyle : Stylesheet() {

    companion object {
        val presentationLabel by cssclass()
        const val basicPadding = 10.0
    }

    init {
        presentationLabel {
            fontSize = 2.em
            fontScale = Font.BOLD
        }
    }
}