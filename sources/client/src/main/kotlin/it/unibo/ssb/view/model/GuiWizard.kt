package it.unibo.ssb.view.model

import it.unibo.ssb.model.Wizard
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import tornadofx.getValue
import tornadofx.setValue
import it.unibo.ssb.model.`Wizard$`.`MODULE$` as WizardCompanion

class GuiWizard(wizard: Wizard = WizardCompanion.apply(DefaultName, DefaultId), cards: Collection<Int> = emptyList()) {

    constructor(name: String = DefaultName, id: Int = DefaultId, cards: Collection<Int> = emptyList()) :
            this(WizardCompanion.apply(name, id), cards)

    val nameProperty: StringProperty = SimpleStringProperty(wizard.name())
    var name: String by nameProperty

    val lifeProperty: IntegerProperty = SimpleIntegerProperty(wizard.lifePoints())
    var life: Int by lifeProperty

    val scoreProperty = SimpleIntegerProperty(0)
    var score by scoreProperty

    var cards: ObservableList<Int> = FXCollections.observableArrayList(cards)

    private companion object {
        private const val DefaultName: String = ""
        private const val DefaultId: Int = 0
    }

}

