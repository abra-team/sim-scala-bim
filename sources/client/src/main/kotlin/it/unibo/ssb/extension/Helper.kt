package it.unibo.ssb.extension

import akka.japi.pf.ReceiveBuilder
import scala.collection.JavaConverters
import scala.collection.Seq
import java.security.SecureRandom

fun <T> Seq<T>.toListK(): List<T> = JavaConverters.seqAsJavaList(this)

fun <T> List<T>.toSeqS(): Seq<T> = JavaConverters.asScalaBuffer(listOf(this).flatten()).toSeq()

/**
 * Extension function of [List], which returns a random element from list
 *
 * @return a random element from [List]
 */
fun <E> List<E>.random(): E = get(SecureRandom().nextInt(size))

fun String.isUseful(): Boolean = this.isNotBlank() && this.isNotEmpty()

fun ReceiveBuilder.match(howToMatch: Function1<ReceiveBuilder, ReceiveBuilder>): ReceiveBuilder = howToMatch(this)
