package it.unibo.ssb.view.frame

import com.github.thomasnield.rxkotlinfx.toObservable
import it.unibo.ssb.view.cardPair
import it.unibo.ssb.view.controller.GameGuiController
import it.unibo.ssb.view.fragments.CardTooltip
import it.unibo.ssb.view.fragments.RoundEndFragment
import it.unibo.ssb.view.fragments.SecretCardFragment
import it.unibo.ssb.view.fragments.SpellCastingOutcomeFragment
import it.unibo.ssb.view.fragments.WizardFragment
import it.unibo.ssb.view.getRandomFace
import it.unibo.ssb.view.style.GameStyle
import it.unibo.ssb.view.style.GameStyle.Companion.basicPadding
import it.unibo.ssb.view.style.GameStyle.Companion.bottomHBox
import it.unibo.ssb.view.style.GameStyle.Companion.buttonChooseSpell
import it.unibo.ssb.view.style.GameStyle.Companion.centerHBox
import it.unibo.ssb.view.style.GameStyle.Companion.circleFill
import it.unibo.ssb.view.style.GameStyle.Companion.circleOpacity
import it.unibo.ssb.view.style.GameStyle.Companion.circleSize
import it.unibo.ssb.view.style.GameStyle.Companion.closeIcon
import it.unibo.ssb.view.style.GameStyle.Companion.endTurnButton
import it.unibo.ssb.view.style.GameStyle.Companion.flowPaneForSpells
import it.unibo.ssb.view.style.GameStyle.Companion.gameFrameLabel
import it.unibo.ssb.view.style.GameStyle.Companion.listUsedSpells
import it.unibo.ssb.view.style.GameStyle.Companion.nextIcon
import it.unibo.ssb.view.style.GameStyle.Companion.playerSize
import it.unibo.ssb.view.style.GameStyle.Companion.quitGameButton
import it.unibo.ssb.view.style.GameStyle.Companion.remainingCards
import it.unibo.ssb.view.style.GameStyle.Companion.topHBoxContainer
import javafx.application.Platform
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.MapChangeListener
import javafx.collections.ObservableList
import javafx.collections.transformation.SortedList
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.stage.Modality
import javafx.stage.StageStyle
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.bind
import tornadofx.borderpane
import tornadofx.button
import tornadofx.circle
import tornadofx.enableWhen
import tornadofx.flowpane
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.listview
import tornadofx.paddingAll
import tornadofx.progressbar
import tornadofx.removeWhen
import tornadofx.singleAssign
import tornadofx.stackpane
import tornadofx.tooltip
import tornadofx.useMaxWidth
import tornadofx.vbox
import tornadofx.vgrow
import it.unibo.ssb.model.`Card$`.`MODULE$` as Card
import it.unibo.ssb.model.`SharedGameBoard$`.`MODULE$` as SharedModelCompanion

/** The view shows what is happening during the game. */
class Game : View(messages["title"]) {

    private val controller: GameGuiController by inject()

    private val players: ObservableList<String> = FXCollections.observableArrayList(controller.players.map { it.name() })

    private var spellOutcome: SpellCastingOutcomeFragment? = null

    private val usedCardsList: ObservableList<Pair<Int, Int>> =
            FXCollections.observableArrayList(controller.usedCards.toList())

    init {
        importStylesheet<GameStyle>()

        players.bind(controller.players) { it.name() ?: "" }

        controller.usedCards.addListener { change: MapChangeListener.Change<out Int, out Int> ->
            Platform.runLater {
                usedCardsList.removeIf { p: Pair<Int, Int> -> p.first == change.key }
                if (change.wasAdded()) {
                    usedCardsList.add(Pair(change.key, change.valueAdded))
                }
            }
        }
    }

    private var secretCards: SecretCardFragment by singleAssign()

    private var topLeftWiz: WizardFragment by singleAssign()
    private var topRightWiz: WizardFragment by singleAssign()
    private var leftWiz: WizardFragment by singleAssign()
    private var rightWiz: WizardFragment by singleAssign()
    private var you: WizardFragment by singleAssign()

    override val root = borderpane {
        hgrow = Priority.ALWAYS
        vgrow = Priority.ALWAYS
        paddingAll = basicPadding
        background = gameBackgroundImage

        top = hbox {
            BorderPane.setAlignment(this, Pos.TOP_CENTER)
            alignment = Pos.CENTER
            addClass(gameFrameLabel)
            label(messages["round"]).addClass(gameFrameLabel)
            label(controller.currentRoundProperty).addClass(gameFrameLabel)
        }

        center = borderpane {
            top = hbox {
                useMaxWidth = true
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                addClass(topHBoxContainer)

                borderpane {
                    topLeftWiz = find(mapOf(
                            WizardFragment::facePath to getRandomFace(),
                            WizardFragment::name to SimpleStringProperty(controller.topLeftWizProperty.value?.name
                                ?: ""),
                            WizardFragment::life to SimpleIntegerProperty(controller.topLeftWizProperty.value?.life
                                ?: 0),
                            WizardFragment::score to SimpleIntegerProperty(controller.topLeftWizProperty.value?.score
                                ?: 0),
                            WizardFragment::cardsOrientation to Orientation.HORIZONTAL,
                            WizardFragment::cards to controller.topLeftWizProperty.value?.cards,
                            WizardFragment::inTurn to controller.wizardInTurnProperty.isEqualTo(
                                    controller.topLeftWizProperty.value?.nameProperty ?: SimpleStringProperty(""))
                    ))
                    center = topLeftWiz.root
                    removeWhen(controller.topLeftWizProperty.isNull)
                    controller.topLeftWizProperty.toObservable().subscribe {
                        topLeftWiz.name.unbind()
                        topLeftWiz.name.bind(it.nameProperty)
                        topLeftWiz.life.unbind()
                        topLeftWiz.life.bind(it.lifeProperty)
                        topLeftWiz.score.unbind()
                        topLeftWiz.score.bind(it.scoreProperty)
                    }
                }

                borderpane {
                    topRightWiz = find(mapOf(
                            WizardFragment::facePath to getRandomFace(),
                            WizardFragment::name to SimpleStringProperty(controller.topRightWizProperty.value?.name
                                ?: ""),
                            WizardFragment::life to SimpleIntegerProperty(controller.topRightWizProperty.value?.life
                                ?: 0),
                            WizardFragment::score to SimpleIntegerProperty(controller.topRightWizProperty.value?.score
                                ?: 0),
                            WizardFragment::cards to controller.topRightWizProperty.value?.cards,
                            WizardFragment::cardsOrientation to Orientation.HORIZONTAL,
                            WizardFragment::inTurn to controller.wizardInTurnProperty.isEqualTo(
                                    controller.topRightWizProperty.value?.nameProperty ?: SimpleStringProperty(""))
                    ))
                    center = topRightWiz.root
                    removeWhen(controller.topRightWizProperty.isNull)
                    controller.topRightWizProperty.toObservable().subscribe {
                        topRightWiz.name.unbind()
                        topRightWiz.name.bind(it.nameProperty)
                        topRightWiz.life.unbind()
                        topRightWiz.life.bind(it.lifeProperty)
                        topRightWiz.score.unbind()
                        topRightWiz.score.bind(it.scoreProperty)
                    }
                }
            }
            center = hbox {
                addClass(centerHBox)
                useMaxWidth = true
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS

                vbox {
                    alignment = Pos.CENTER_LEFT
                    label("${messages["used_spells"]}:").addClass(gameFrameLabel)
                    listview(SortedList(usedCardsList).sorted { p1, p2 -> p1.first - p2.first }) {
                        addClass(listUsedSpells)
                        cellFormat {
                            graphic = hbox {
                                spacing = basicPadding * 2
                                label(it.first.toString())
                                progressbar(it.second.toDouble() / it.first.toDouble())
                                label("${it.second} ${messages["out_of"]} ${it.first}")
                            }
                            isEditable = false
                            isMouseTransparent = true
                            isFocusTraversable = false
                        }
                        placeholder = label("No cards in deck")
                        isEditable = false
                    }
                }

                secretCards = find(mapOf(SecretCardFragment::secretCardSize to controller.secretCardsSizeProperty))
                add(secretCards)

                vbox {
                    addClass(centerHBox)
                    label("${messages["remaining_cards"]}:").addClass(gameFrameLabel)
                    stackpane {
                        circle {
                            centerX = playerSize
                            centerY = playerSize
                            radius = circleSize
                            fill = circleFill
                            opacity = circleOpacity
                        }
                        label(controller.deckSizeProperty.asString()) {
                            addClass(gameFrameLabel)
                            addClass(remainingCards)
                        }
                    }
                }

            }
            bottom = hbox {
                addClass(bottomHBox)
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS

                val yourTurn = controller.wizardInTurnProperty.isEqualTo(controller.thisWizProperty.value?.nameProperty)

                you = find(mapOf(
                        WizardFragment::facePath to getRandomFace(),
                        WizardFragment::name to SimpleStringProperty(controller.thisWizProperty.value?.name ?: ""),
                        WizardFragment::life to SimpleIntegerProperty(controller.thisWizProperty.value?.life ?: 0),
                        WizardFragment::cards to controller.thisWizProperty.value?.cards,
                        WizardFragment::score to SimpleIntegerProperty(controller.thisWizProperty.value?.score ?: 0),
                        WizardFragment::cardsOrientation to Orientation.HORIZONTAL,
                        WizardFragment::inTurn to yourTurn
                ))
                controller.thisWizProperty.toObservable().subscribe {
                    you.name.unbind()
                    you.name.bind(it.nameProperty)
                    you.life.unbind()
                    you.life.bind(it.lifeProperty)
                    you.score.unbind()
                    you.score.bind(it.scoreProperty)
                }
                add(you)

                vbox {
                    addClass(centerHBox)
                    label("${messages["choose_spell"].toUpperCase()}!").addClass(gameFrameLabel)
                    flowpane {
                        addClass(flowPaneForSpells)
                        for (i in 1..Card.NumberOfSpells()) {
                            button("$i") {
                                addClass(buttonChooseSpell)
                                enableWhen(yourTurn)
                                tooltip {
                                    graphic = find<CardTooltip>(mapOf(CardTooltip::currentPair to cardPair(i))).root
                                }
                                action {
                                    controller.wizardCastsSpell(i)
                                }
                            }
                        }
                    }
                }

                vbox {
                    addClass(centerHBox)
                    button(messages["end_turn"].toUpperCase()) {
                        addClass(endTurnButton)
                        enableWhen(yourTurn)
                        graphic = nextIcon()
                        action {
                            controller.wizardSkipsTurn()
                        }
                    }
                    button(messages["quit_game"].toUpperCase()) {
                        addClass(quitGameButton)
                        enableWhen(yourTurn)
                        graphic = closeIcon()
                        action {
                            controller.wizardQuitsGame()
                        }
                    }
                }
            }
            right = hbox {
                rightWiz = find(mapOf(
                        WizardFragment::facePath to getRandomFace(),
                        WizardFragment::name to SimpleStringProperty(controller.rightWizProperty.value?.name ?: ""),
                        WizardFragment::life to SimpleIntegerProperty(controller.rightWizProperty.value?.life ?: 0),
                        WizardFragment::score to SimpleIntegerProperty(controller.rightWizProperty.value?.score ?: 0),
                        WizardFragment::cardsOrientation to Orientation.HORIZONTAL,
                        WizardFragment::cards to controller.rightWizProperty.value?.cards,
                        WizardFragment::inTurn to controller.wizardInTurnProperty.isEqualTo(
                                controller.rightWizProperty.value?.nameProperty ?: SimpleStringProperty(""))
                ))
                add(rightWiz)
                removeWhen(controller.rightWizProperty.isNull)
                controller.rightWizProperty.toObservable().subscribe {
                    rightWiz.name.unbind()
                    rightWiz.name.bind(it.nameProperty)
                    rightWiz.life.unbind()
                    rightWiz.life.bind(it.lifeProperty)
                    rightWiz.score.unbind()
                    rightWiz.score.bind(it.scoreProperty)
                }
            }
            left = hbox {
                leftWiz = find(mapOf(
                        WizardFragment::facePath to getRandomFace(),
                        WizardFragment::name to SimpleStringProperty(controller.leftWizProperty.value?.name ?: ""),
                        WizardFragment::life to SimpleIntegerProperty(controller.leftWizProperty.value?.life ?: 0),
                        WizardFragment::score to SimpleIntegerProperty(controller.leftWizProperty.value?.score ?: 0),
                        WizardFragment::cardsOrientation to Orientation.HORIZONTAL,
                        WizardFragment::cards to controller.leftWizProperty.value?.cards,
                        WizardFragment::inTurn to controller.wizardInTurnProperty.isEqualTo(
                                controller.leftWizProperty.value?.nameProperty ?: SimpleStringProperty(""))
                ))
                add(leftWiz)
                removeWhen(controller.leftWizProperty.isNull)
                controller.leftWizProperty.toObservable().subscribe {
                    leftWiz.name.unbind()
                    leftWiz.name.bind(it.nameProperty)
                    leftWiz.life.unbind()
                    leftWiz.life.bind(it.lifeProperty)
                    leftWiz.score.unbind()
                    leftWiz.score.bind(it.scoreProperty)
                }
            }
        }
    }

    /**
     * Function that will be called by the [GameGuiController], which will open a
     * [SpellCastingOutcomeFragment] as internal window, that will notify the user of a
     * spell casted by other wizards or the outcome of a spell casted by himself.
     *
     * @param spellToCast   the id of the spell casted
     * @param hasCasted     the actual outcome of the spell casting
     * @param wizard        the wizard who casted the spell
     */
    fun notifyCastOutcome(spellToCast: Int, hasCasted: Boolean,
                          wizard: String = defaultWizard, cardDrawn: Int? = null) {
        spellOutcome?.close()
        spellOutcome = find(mapOf(SpellCastingOutcomeFragment::outcome to hasCasted,
                SpellCastingOutcomeFragment::spell to spellToCast,
                SpellCastingOutcomeFragment::wizardName to wizard,
                SpellCastingOutcomeFragment::cardDrawn to cardDrawn))
        spellOutcome?.openModal(stageStyle = StageStyle.UNDECORATED,
                modality = Modality.APPLICATION_MODAL, escapeClosesWindow = false)
    }

    /**
     * Function to be called by [GameGuiController]: it opens a [RoundEndFragment] as
     * internal window, which will display the round end information.
     *
     * @param winners   the list of the names of the winners that have won the round
     * @param round     the round that has just been won
     * @param ranking   the current round ranking
     */
    fun notifyRoundEnd(winners: List<String>, round: Int, ranking: List<Pair<String, Int>>) {
        openRoundWindow(winners, round, ranking, false)
    }

    /**
     * Function to be called by [GameGuiController]: it opens a [RoundEndFragment] relative
     * to the game end as internal window, which will display the game end information.
     *
     * @param winners   the list of the names of the winners that have won the game
     * @param ranking   the final game ranking
     */
    fun notifyGameEnd(winners: List<String>, ranking: List<Pair<String, Int>>) {
        openRoundWindow(winners, controller.currentRound, ranking, true)
    }

    /**
     * Function that opens a window to notify the end of the round or the game.
     *
     * @param winners       a list of the winners
     * @param round         the round that has just ended
     * @param isGameOver    true if the game is over, false otherwise
     */
    private fun openRoundWindow(winners: List<String>, round: Int,
                                ranking: List<Pair<String, Int>>, isGameOver: Boolean) {
        spellOutcome?.close()
        find<RoundEndFragment>(mapOf(RoundEndFragment::gameOver to isGameOver,
                RoundEndFragment::wizards to winners,
                RoundEndFragment::round to round,
                RoundEndFragment::ranking to ranking))
                .openModal(stageStyle = StageStyle.UNDECORATED, block = true)
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
        currentWindow?.centerOnScreen()
    }

    companion object {
        private const val backgroundPath = "it/unibo/ssb/backgrounds/"
        private const val mainBackground = "Background1.png"
        const val defaultWizard = "Me"

        private val gameBackgroundImage = Background(
                BackgroundImage(Image(backgroundPath + mainBackground),
                        BackgroundRepeat.REPEAT,
                        BackgroundRepeat.REPEAT,
                        BackgroundPosition.DEFAULT,
                        BackgroundSize(BackgroundSize.AUTO,
                                BackgroundSize.AUTO,
                                false,
                                false,
                                false, true)))
    }
}
