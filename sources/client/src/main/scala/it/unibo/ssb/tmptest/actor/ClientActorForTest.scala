package it.unibo.ssb.tmptest.actor

import akka.actor.Props
import it.unibo.ssb.controller.messages.{ClientMessage, Message}
import it.unibo.ssb.controller.messages.ClientMessage.PlayerWantsToStartGame
import it.unibo.ssb.controller.messages.GameMessage.{GetGameBoardMessage, WizardHasWonTheRound}
import it.unibo.ssb.controller.messages.LobbyMessage.Setting
import it.unibo.ssb.controller.messages.{ClientMessage, Message}
import it.unibo.ssb.controller.ClientActor
import it.unibo.ssb.model.Wizard
import it.unibo.ssb.model.lobby.Settings

class ClientActorForTest extends ClientActor() {


  override def receive: Receive = {
    case m: ClientMessage =>

      receiveClientMsg(m)
    case m: Message =>
      super.receive(m)
  }

  override protected def receiveClientMsg(m: ClientMessage): Unit = m match {
    case PlayerWantsToStartGame(auth, isRanked, maxPlayers, _) =>
      createLobby(isRanked, maxPlayers, auth.username)
      if(auth == null) createLobby(isRanked, maxPlayers, "Anonimous")
      else createLobby(isRanked, maxPlayers, auth.username)
    case _ => super.receiveClientMsg(m)
  }

  private def createLobby(isRanked: Boolean, maxPlayers: Int, name: String): Unit = {
    val lobby = context.actorOf(Props(new LobbyForTest(self)))
    val setting: Settings = Settings((self, name), isRanked, maxPlayers, maxPlayers - 1)
    lobby ! Setting(setting)
  }
}
