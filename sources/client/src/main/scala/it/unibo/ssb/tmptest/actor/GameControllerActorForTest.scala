package it.unibo.ssb.tmptest.actor

import java.util.{Timer, TimerTask}

import akka.actor.{ActorRef, Stash}
import it.unibo.ssb.controller.ControlActor
import it.unibo.ssb.controller.game.{GameController, GameControllerInitializer}
import it.unibo.ssb.controller.messages.GameMessage._
import it.unibo.ssb.model.{Card, GameBoard, Wizard}

import scala.collection.mutable.ListBuffer

/**
  * actor that has the job to interact with the GameGUIActor via message.
  *
  * @param ranked  the attribute of the controller that represents weather the game is ranked or not.
  * @param wizards the name of the wizards.
  */
class GameControllerActorForTest(ranked: Boolean, wizards: List[String])
  extends ControlActor with Stash with GameControllerInitializer {

  private val gameController = Some(GameController(wizards, ranked))
  private val clientActors = ListBuffer.empty[(ActorRef, String)]
  private var asyncTimer = new Timer()
  private var roundMsgCounter = 0
  private final val sleep = 1500
  private final val owl = 4
  wizards.foreach(w => "GCA: Winzard received: " + w)
  //private var leaderBoard : Option[LeaderBoard]= if(ranked){ Some(LeaderBoard()) } else None

  //noinspection ScalaStyle
  override def receive: Receive = {
    case InitializeGameMessage =>
      if (gameController.get.wizardsPlayer.nonEmpty) {
        initializeGame()
      }
    case WizardsCastSpellMessage(wizard, spellToCast, canCast) =>
      if (canCast.isDefined) logger.warn("Unused canCast: " + canCast.get)
      wizardsCastSpell(wizard, spellToCast)
    case NextWizardMomentMessage() => nextWizardMoment()
    case GetGameBoardMessage(gameBoard) =>
      if (gameBoard.isDefined) logger.warn("Unused gameboard: " + gameBoard.get)
      getGameBoard
    case WizardTurnMessage(wizardInTurn) =>
      if (wizardInTurn.isDefined) logger.warn("Unused wizard: " + wizardInTurn.get)
      wizardTurn
    case TimerTimeOut() => handleDisconnection(gameController.get.wizardTurn)
    case ClientPresentation(ref, name) =>
      clientActors += ((ref, name))
      if (clientActors.size == wizards.size) {
        initializeGame()
      }
    case StartRound(_) => roundHandling()
    case ContinueTheGame() =>
  }

  override def initializeGame(): Unit = {
    gameController.get.initializeGame()
    startGame()
  }

  override def startGame(): Unit = {
    gameController.get.startGame()
    clientActors.toStream.foreach(it => it._1 ! StartGameMessage())
    wizardTurn
  }

  override def wizardTurn: Wizard = {
    val wizardInTurn = gameController.get.wizardTurn
    clientActors.toStream.foreach(it => it._1 ! WizardTurnMessage(Option(wizardInTurn)))
    startTimer()
    wizardInTurn
  }

  override def wizardsCastSpell(wizard: Wizard, spellToCast: Int): Boolean = {
    val wizardTmp = getGameBoard.livingWizard.filter(_.name == wizard.name).head
    var canCast: Boolean = false
    // stop timer
    stopTimer()

    var cards = List[Card]()
    if (spellToCast == 4) {
      cards = getGameBoard.wizardGameBoard(wizard.name).cardsList.toList
      canCast = gameController.get.wizardsCastSpell(wizardTmp, spellToCast)
      if (canCast) {
        val card: Card = getGameBoard.wizardGameBoard(wizardTmp.name).cardsList.:+(Card(owl)).diff(cards).head
        clientActors.filter(_._2 == wizardTmp.name).foreach(_._1 ! SecretCard(card.id))
      }
    } else {
      // try that spell on the model
      canCast = gameController.get.wizardsCastSpell(wizardTmp, spellToCast)
    }

    // communicate to all clients what append
    clientActors.toStream.foreach(it => it._1 ! WizardsCastSpellMessage(wizardTmp, spellToCast, Option(canCast)))

    //check if some has won
    val isEnd = handleWinners(gameController.get.getWinners)
    if (canCast && !isEnd) {
      wizardTurn
      canCast
    } else {
      if (!isEnd) nextWizardMoment()
      canCast
    }
  }

  override def nextWizardMoment(): Unit = {

    stopTimer()
    gameController.get.nextWizardMoment()
    clientActors.foreach(p => p._1 ! NextWizardMomentMessage())
    wizardTurn
  }

  override def getGameBoard: GameBoard = {
    Thread.sleep(sleep)
    val gameboard: GameBoard = gameController.get.getGameBoard
    clientActors.toStream.foreach(it => it._1 ! GetGameBoardMessage(Option(gameboard)))
    gameboard
  }

  override def setPlayer(numberOfHumanPlayers: List[String]): Unit = {
    gameController.get.setPlayer(numberOfHumanPlayers)
    clientActors.toStream.foreach(it => it._1 ! SetPlayerMessage(numberOfHumanPlayers))
  }

  override def wizardsPlayer: Seq[Wizard] = {
    val players = gameController.get.wizardsPlayer
    clientActors.toStream.foreach(it => it._1 ! WizardsPlayerMessage(players))
    players
  }

  private def task = new TimerTask {
    override def run(): Unit = self ! TimerTimeOut()
  }

  private def startTimer(): Unit = {
    asyncTimer.schedule(task, 100 * 1000)
  }

  private def stopTimer(): Unit = {
    asyncTimer.cancel()
    asyncTimer = new Timer()
  }

  private def handleDisconnection(wizardDisconnected: Wizard): Unit = {
    stopTimer()

    // i communicate to each clients that he has quit
    clientActors.foreach(_._1 ! PlayerDisconnection(wizardDisconnected))
    // If there is just one player left he wins the turn
    if (gameController.get.getGameBoard.livingWizard.size == 2) {
      // i remove wizard disconnected
      gameController.get.wizardDisconnected(wizardDisconnected)
      handleWinners(Option(gameController.get.getGameBoard.livingWizard.toList))
    } else {
      // i tell to next wizard it's his turn
      nextWizardMoment()
      // i remove wizard disconnected
      gameController.get.wizardDisconnected(wizardDisconnected)
    }
  }

  private def handleWinners(winners: Option[List[Wizard]]): Boolean = {
    if (winners.nonEmpty) {
      clientActors.foreach(_._1 ! WizardHasWonTheRound(winners.get, gameController.get.currentRound,
        gameController.get.getRanking))
    }
    winners.nonEmpty
  }

  private def roundHandling(): Unit = {
    roundMsgCounter += 1
    if (roundMsgCounter == clientActors.size) {
      roundMsgCounter = 0
      nextRound()
    }
  }

  private def nextRound(): Unit = {
    gameController.get.nextRound()
    if (gameController.get.gameWinners.nonEmpty) {
      gameEndHandling(gameController.get.gameWinners)
    } else {
      wizardTurn
    }
  }

  private def gameEndHandling(ranking: List[(Wizard, Boolean, Int)]): Unit = {
    clientActors.foreach(_._1 ! WizardHasWonTheGame(ranking.filter(_._2).map(_._1), ranking.map(w => (w._1, w._3))))
  }

}

