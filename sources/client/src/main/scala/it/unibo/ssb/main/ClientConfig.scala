package it.unibo.ssb.main

import java.net.URL
import java.security.SecureRandom

import akka.actor.Address
import better.files.Resource
import com.typesafe.config.{ConfigException, Config => AkkaConfig, ConfigFactory => AkkaConfigFactory}
import it.unibo.ssb.controller.SingleActorSystemWrapper
import it.unibo.ssb.main.ClientConfig.{MAX_RAND_NUM, MIN_RAND_NUM}
import it.unibo.ssb.model.remote
import it.unibo.ssb.model.remote.DefaultApiPort

/**
  * This class models a configuration for the server execution.
  *
  * @param akkaHostname       the hostname the local actorSystem should use
  * @param akkaPort           the port the local actorSystem should use
  * @param remoteAkkaHostname the hostname the actorSystem on server should be using
  * @param remoteAkkaPort     the port the actorSystem on server should be using
  * @param remoteApiHostname  the hostname the HTTP server should be using
  * @param remoteApiPort      the port the HTTP server should be using
  *
  * @see [[it.unibo.ssb.main.ClientParser]]
  * @see [[https://github.com/scopt/scopt#usage]]
  */
case class ClientConfig(
    akkaHostname: String = ClientConfig.DefaultAkkaHostname,
    akkaPort: Int = ClientConfig.DefaultAkkaPort,
    remoteAkkaHostname: String = remote.DefaultHostNameServer,
    remoteAkkaPort: Int = remote.DefaultServerAkkaPort,
    remoteApiHostname: String = remote.DefaultHostNameServer,
    remoteApiPort: Int = DefaultApiPort
) {
  // Non-static, overridable pre-defined properties for the ClientConfig
  /** The name of the client actorSystem. */
  val systemName: String = (SecureRandom.getInstanceStrong.nextInt(MAX_RAND_NUM - MIN_RAND_NUM) + MIN_RAND_NUM).toString
  /** The base address of the client actorSystem. */
  def address: Address = Address("akka.tcp", SingleActorSystemWrapper.INSTANCE.getSystem.name, akkaHostname, akkaPort)
}

object ClientConfig {
  private final val MAX_RAND_NUM: Int = 999999999
  private final val MIN_RAND_NUM: Int = 100000000

  /** Base Akka key for remote TCP configurations. */
  private final val AkkaRemoteNettyTcp: String = "akka.remote.netty.tcp"
  /** Akka key for hostname. */
  final val AkkaLogicalHostname: String = s"$AkkaRemoteNettyTcp.hostname"
  /** Akka key for port. */
  final val AkkaLogicalPort: String = s"$AkkaRemoteNettyTcp.port"
  /** Akka key for bounded hostname. */
  final val AkkaBindHostname: String = s"$AkkaRemoteNettyTcp.bind-hostname"
  /** Akka key for bounded port. */
  final val AkkaBindPort: String = s"$AkkaRemoteNettyTcp.bind-port"

  /** Akka configuration file name. */
  private final val fileName: String = "application.conf"

  /** Akka possible configuration file.
    *
    * It may be non-present (ie. in test environments).
    */
  private final val confFile: URL = Resource.getUrl(fileName)

  /**
    * Local Akka [[akka.actor.ActorSystem ActorSystem]] basic configuration.
    *
    * @see [[https://doc.akka.io/docs/akka/2.5/general/configuration.html]]
    */
  val akkaConfig: AkkaConfig = AkkaConfigFactory.parseURL(confFile)

  /** Parse default Akka hostname from configuration file, if it defines it. */
  private val DefaultAkkaHostname: String = try {
    akkaConfig.getString(ClientConfig.AkkaLogicalHostname)
  } catch {
    case _: ConfigException.Missing => remote.DefaultHostname
    case e: Throwable => throw e
  }

  /** Parse default Akka port from configuration file, if it defines it. */
  private val DefaultAkkaPort: Int = try {
    akkaConfig.getString(ClientConfig.AkkaLogicalPort).toInt
  } catch {
    case _: ConfigException.Missing => remote.DefaultAkkaPort
    case e: Throwable => throw e
  }
}
