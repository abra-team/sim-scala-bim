package it.unibo.ssb.actors

import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.testkit.{ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import it.unibo.ssb.controller.game.GameControllerSimple.MaximumPlayer
import it.unibo.ssb.controller.lobby.MasterLobbyActor
import it.unibo.ssb.controller.messages.DiscoveryMessage.Error
import it.unibo.ssb.controller.messages.LobbyMessage.Hello
import it.unibo.ssb.controller.messages.MasterLobbyMessage.CreateThisLobby
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuiteLike, Matchers}

class MasterLobbyTest extends TestKit(ActorSystem("MySpec", ConfigFactory.parseString("akka {\n  loglevel = \"INFO\"\n" +
  "  actor {\n    provider = \"akka.remote.RemoteActorRefProvider\"\n  }\n" +
  "  remote {\n    enabled-transports = [\"akka.remote.netty.tcp\"]\n" +
  "    netty.tcp {\n      hostname = \"127.0.0.1\"\n      port = 5152\n    }\n" +
  "    log-sent-messages = on\n    log-received-messages = on\n  }\n}"))) with FunSuiteLike
  with ImplicitSender with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  var ml: ActorRef = _
  private final val player = "me"

  override def beforeEach(): Unit = {
    ml = system.actorOf(MasterLobbyActor.props())
  }

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  test("MasterLobby must be contactable") {
    ml ! "Hello"
    val msg = receiveN(1).head
    assert(msg.isInstanceOf[Error])
    ml ! PoisonPill
  }

  test("I can't start game whit a bad configuration") {

    // i can't create a game with the number of players greater than allowed
    ml ! CreateThisLobby(MaximumPlayer + 1, isRanked = false, player, None)
    assert(receiveN(1).head.isInstanceOf[Error])

    // i can't create a game with list of invited friends greater the a number of player
    val friends: List[String] = List[String]("Andrea", " gabriele", "Alfonso")
    ml ! CreateThisLobby(2, isRanked = false, player, Some(friends))
    assert(receiveN(1).head.isInstanceOf[Error])
    // i cant create a ranked game with invited friends
    ml ! CreateThisLobby(MaximumPlayer, isRanked = true, player, Some(friends))
    assert(receiveN(1).head.isInstanceOf[Error])
    // i cant create a ranked game with different number of plyers
    ml ! CreateThisLobby(MaximumPlayer - 1, isRanked = true, player, None)
    assert(receiveN(1).head.isInstanceOf[Error])
    ml ! PoisonPill

  }

  test("I can create a normal game correctly") {
    ml ! CreateThisLobby(MaximumPlayer, isRanked = false, player, None)
    assert(receiveN(1).head.isInstanceOf[Hello])
    ml ! PoisonPill
  }


}
