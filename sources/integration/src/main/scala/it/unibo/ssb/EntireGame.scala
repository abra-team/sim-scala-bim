package it.unibo.ssb

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import it.unibo.ssb.controller.messages.GameMessage.{ClientPresentation, GetGameBoardMessage,
  NextWizardMomentMessage, PlayerDisconnection, StartGameMessage, StartRound, WizardHasWonTheGame,
  WizardHasWonTheRound, WizardTurnMessage, WizardsCastSpellMessage}
import it.unibo.ssb.model.{GameBoard, Wizard}
import it.unibo.ssb.controller.GameControllerActor

import scala.collection.mutable.ListBuffer

//noinspection ScalaStyle
case class EntireGame(system: ActorSystem) extends Actor {

  private val preBuildGame = ListBuffer[String]("Lia", "leo", "gio", "mausy", "fred")
  private val players = ListBuffer.empty[String]
  private val disconnected = ListBuffer.empty[String]
  private var gca: Option[ActorRef] = None
  private var state = StateValue.start
  private var counterWinningMessage = 0
  private var counterClientReady = 0
  private var counterTurnMsg = 0
  private var counterGameBoardMsg = 0
  private var counterWizardCastSpellMsg = 0
  private var counterWizardDisconnection = 0
  private var wizardMoment: Option[Wizard] = None

  override def receive: Receive = {
    case "start" => startGame()
    case StartGameMessage() => startGameHandling()
    case WizardTurnMessage(wizard) => turnHandling(wizard.get)
    case GetGameBoardMessage(gameboard) => gameBoardHandling(wizardMoment.get, gameboard.get)
    case WizardsCastSpellMessage(wizard, spell, isCasted) => spellCastingHandling(wizard,spell, isCasted.get)
    case WizardHasWonTheRound(wizards, round, ranking) => someoneHasWonHandling(wizards, round, ranking)
    case WizardHasWonTheGame(_, ranking) => someoneHasWonTheGame(ranking)
    case PlayerDisconnection(wizardDisconnected) => handleDisconnection(wizardDisconnected)
  }

  def startGame(): Unit = {
    promptCMDLineMenu()
  }

  def promptCMDLineMenu(): Unit = {
    println("#" * 50)
    println("\t\tSim-scala-binV1")
    println("#" * 50)
    println("\n\n")
    println("Select:")
    println("1 - Start Normal Game")
    println("2 - Start Ranked Game")
    val choise = scala.io.StdIn.readLine(":>")


    var go = true

    while (go) {
      println("#" * 50)
      println("\t\tSim-scala-binV1 \t NofPlayer: " + players.size)
      println("#" * 50)
      println("\n\n")
      println("Select:")
      println("1 - Enter a player")
      println("2 - Start Game")
      println("3 - start a pre build game")

      val choise = scala.io.StdIn.readLine(":> ")
      if (choise == "1") {
        println("Enter player name: ")
        val name = scala.io.StdIn.readLine(":> ")
        players += name
      } else if(choise == "2") {
        go = false
      }else{
        players ++= preBuildGame
        go = false
      }
    }

    val game = choise match {
      case "1" => system.actorOf(GameControllerActor.props(ranked = false, players.toList))
      case "2" => system.actorOf(GameControllerActor.props(ranked = true, players.toList))
    }

    gca = Some(game)

    state = StateValue.clientsPresentation
    players.foreach(p => gca.get ! ClientPresentation(self, p))
    Thread.sleep(500)

  }

  private def startGameHandling(): Unit = {
    counterClientReady += 1
    if (counterClientReady == players.size && state == StateValue.clientsPresentation) {
      println("Server knows all clients. We are ready to start!!")
      state = StateValue.waitingTurn
      counterClientReady = 0
    }
  }

  private def turnHandling(wizard: Wizard): Unit = {
    counterTurnMsg += 1
    println("884: A Turn msg received with: "+ wizard.name)
    if (counterTurnMsg == players.size && state == StateValue.waitingTurn) {
      println("91: All Turn msg received")
      state = StateValue.wizardMoment
      wizardMoment = Option(wizard)
      counterTurnMsg = 0
      gca.get ! GetGameBoardMessage(None)
    }
  }

  private def spellCastingHandling(wizard: Wizard, spell: Int, isCasted: Boolean ): Unit = {
    counterWizardCastSpellMsg += 1
    if(counterWizardCastSpellMsg == players.size && state == StateValue.wizardMoment
      && wizard == wizardMoment.get){
      counterWizardCastSpellMsg = 0
      val view = if (isCasted) {
        "SPELL: " + spell + " casted correctly!!"
      } else {
        "SPELL: " + spell + "is not in your pool :("
      }
      println(view)
      state = StateValue.waitingTurn

    }
  }

  private def gameBoardHandling(wizard: Wizard, gb: GameBoard):Unit = {
    counterGameBoardMsg += 1
    println("118: game board msg received")
    if (counterGameBoardMsg == players.size && state == StateValue.wizardMoment) {
      counterGameBoardMsg = 0
      playTurnOf(wizard, gb)
    }
  }

  private def playTurnOf(wizard: Wizard, lastGameBoard: GameBoard): Unit = {
    var view = s"############ " + wizard.name + " #############"
    view += "\nTURN: " + wizardMoment.get.name
    view += "\nHitPoints: " + lastGameBoard.livingWizard.filter(_ == wizard).head
      .asInstanceOf[Wizard].lifePoints
    view += " CARDS: " + "[]" * lastGameBoard.wizardGameBoard(wizard).cardsList.size
    lastGameBoard.livingWizard.filter(_ != wizardMoment.get).foreach(w => {
      view += "\nID: " + (lastGameBoard.livingWizard.indexOf(w) + 1) + " " + w.name
      view += " " + w.lifePoints
      view += " CARDS: " + lastGameBoard.wizardGameBoard(w)
        .cardsList
        .map(c => " " + c.name + " : " + c.id)
        .mkString
    })
    view += "\n\n ----------- SHARED GAME BOARD ----------- "
    view += "\n\nCARDS: " + lastGameBoard.sharedGameBoard.deckSize
    view += "\nUSED CARD: " + lastGameBoard.sharedGameBoard.usedCards
      .map(c => "C" + c.id).mkString
    view += "\nSECRET POOL: " + lastGameBoard.sharedGameBoard.secretCardsSize()
    view += "\nIS YOUR TURN: \n 1. Cast spell\n 2. Next Wizard \n3.Disconnect:> "
    println(view)
    val choise = scala.io.StdIn.readLine()
    if(choise == "1") castSpell()
    else if(choise == "2") nextWizardMoment()
    else {
      state = StateValue.waitingTurn
    }
  }

  private def castSpell(): Unit = {
    println("Number of spell you wish to cast[1-8]: ")
    val spell = scala.io.StdIn.readLine()
    gca.get ! WizardsCastSpellMessage(wizardMoment.get, spell.toInt, None)
  }

  private def nextWizardMoment(): Unit = {
    state = StateValue.waitingTurn
    gca.get ! NextWizardMomentMessage()
  }

  private def someoneHasWonHandling(winners: List[Wizard], round: Int, ranking: List[(Wizard, Int)] ): Unit ={
    counterWinningMessage += 1
    if(counterWinningMessage == players.size){
      counterWinningMessage = 0
      var view = "#"*30+ "\n"
      view += ("#" + " "*28 + "#\n")*5
      view += s"#    Winners of round $round :    #\n"
      winners.foreach(w => {
        view += "#" + " "*((28 - w.name.length)/2)+ w.name + " "*((28 - w.name.length)/2)+"#\n"
      })
      view += ("#" + " "*28 + "#\n")*5
      view += "#"*30+ "\n"
      view += "\n\nPress enter to play next round or 1 to see the current ranking"
      println(view)
      val choise = scala.io.StdIn.readLine()
      if(choise == "1"){
        println("RANKING: \n"+ranking)
      }
      println("Let's play")
      for(_ <- 1 to players.size) gca.get ! StartRound(round)
      state = StateValue.waitingTurn
    }


  }

  private def someoneHasWonTheGame(ranking: List[(Wizard, Int)]): Unit ={
    var view = "------- END GAME ---------"
    view += "\n\n WINNERS: "
    view += ranking.map(w => ""+w._1.name + ", Score: "+ w._2+ "\n").mkString("")
    println(view)
  }

  private def handleDisconnection(wizard: Wizard): Unit ={
    counterWizardDisconnection += 1
    println("200:Disconnection msg")
    if(counterWizardDisconnection == players.size){
      counterWizardDisconnection = 0
      disconnected += wizard.name
      players.remove(players.indexOf(wizard.name))
      showDisconnection(wizard)
      println("Actual Players: " + players)
    }

  }

  private def showDisconnection(wizard: Wizard): Unit ={
    println("*"*60)
    println("\t\tATTENTION:")
    println(s"\t${wizard.name} : ${wizard.id} has disconnected")
    println("*"*60)
    scala.io.StdIn.readLine()
  }



  case object StateValue {
    val waitingTurn = "waitforturn"
    val wizardMoment = "WizardMoment"
    val clientsPresentation = "ClientsPresentation"
    val start = "GameStarting"
  }


}

object EntireGame {
  def props(system: ActorSystem): Props = Props(new EntireGame(system))
}


object Game extends App {
  val system = ActorSystem("SIM_SCALA_BIN")
  val game = system.actorOf(EntireGame.props(system))
  game ! "start"
}
