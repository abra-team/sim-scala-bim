package it.unibo.ssb.controller

import akka.actor.Actor
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.controller.game.GameControllerForPlay

/**
  * Trait for control actor for the game
  */
trait ControlActor extends Actor with GameControllerForPlay with LazyLogging
