package it.unibo.ssb.controller.game

import scala.math.pow

trait ScoreRanking {
  /**
    * Method that computes the offset to add to the old score
    *
    * @param winnerScores List of winning scores
    * @param otherScores  List of Other scores
    *
    * @return (Off, Off):offset to add to winner, Offset to remove to loser
    */
  def computeScore(winnerScores: List[Int], otherScores: List[Int]): (Double, Double)
}

object ScoreRanking {

  def apply(): ScoreRanking = ScoreRankingSimple(algorithmV1)

  /**
    *
    * This algorithm is not optimized for multi-winner game.
    *
    * At the core there is ELO System algorithm for chess ([[https://www.geeksforgeeks.org/elo-rating-algorithm]])
    * To compute new score for a player ELO Sys. Alg. works following this steps:
    * <ol>
    * <li> Compute K factor: if score < 2400 k = 30 otherwise k = 20 ( this is not standard )
    * <li> Apply for each player this formula P = (1.0 / (1.0 + pow(10, ((score1 – score2) / 400))))
    * <ul><li>where P is the probability for player to win
    * <li>pow(x,y) = x elevated by y
    * </ul>
    * <li> Compute scores for each player using this formula newscore = oldscore + K*(isWinner – P)
    * <ul>
    * <li> isWinner is 1 if player has win otherwise 0
    * <li> oldscore is the score before the match
    * </ul>
    * </ol>
    * Obviously chess is a two players game and Sim-scala-bho is multi-player game
    * <br>
    * This version of algorithm has in its core ELO System Algorithm.
    * It reduces the dimension doing means between scores<br>
    * It works according to this next few steps:
    * <ol>
    * <li>Compute means of winners score (winnerMeanScore)
    * <li> Compute means of other player (meanScores)
    * <li> Compute K value using mean
    * <li> Apply elo system algorithm using winnerMeanScore
    * <li> return the offset between winnerMeanScore and new Score for winner and the same for losers
    * </ol>
    *
    * @param winner List of winning scores
    * @param others List of Other scores
    *
    * @return (Off, Off):offset to add to winner, Offset to remove to loser
    */
  def algorithmV1(winner: List[Int], others: List[Int]): (Double, Double) = {
    val winnerMeanScore = winner.sum / winner.size
    val meanScore = others.sum / others.size

    // compute K
    def k = winnerMeanScore compare Value.splitForKCalculation match {
      case 1 => Value.minorKValue
      case _ => Value.maxKVale
    }

    // compute P for winner
    def wP = 1.0 / (1.0 + pow(Value.baseForP, (meanScore - winnerMeanScore) / Value.divisorFactor))

    // compute P for loser
    def lP = 1.0 / (1.0 + pow(Value.baseForP, (winnerMeanScore - meanScore) / Value.divisorFactor))


    // compute winner score
    val winnerScoreOffset = (k * (Value.winnerFactor - wP))/winner.size
    val loserScoreOffset = (k * (Value.loserFactor - lP))/others.size


    (winnerScoreOffset, loserScoreOffset)

  }

  case class ScoreRankingSimple(func: (List[Int], List[Int]) => (Double, Double)) extends ScoreRanking {
    override def computeScore(winnerScores: List[Int], otherScores: List[Int]): (Double, Double) = func(winnerScores, otherScores)
  }

  /** \textsl{}
    * This object contains factor and value used by algorithms
    */
  case object Value {
    val splitForKCalculation = 2400
    val minorKValue = 20
    val maxKVale = 30
    val divisorFactor = 400.0
    val baseForP = 10.0
    val winnerFactor = 1.0
    val loserFactor = 0.0
  }

}
