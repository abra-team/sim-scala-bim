package it.unibo.ssb.model

trait Wizard extends Serializable {

  /**
    *
    * @return the name of the wizard
    */
  def name: String

  /**
    *
    * @param value of point to add to the wizard life (cannot be > of the MAX)
    */
  def incLife(value: Int = 1):Unit

  /**
    *
    * @param value of point to dec to the wizard life (cannot be < of 0)
    */
  def decLife(value: Int = 1):Unit

  /**
    *
    * @return the ID od the wizard
    */
  def id: Int

  /**
    *
    * @return true if the lifePoints of the wizard are > 0, else false
    */
  def isAlive: Boolean

  /**
    *
    * @return true if the lifePoints of the wizard are = 0, else false
    */
  def isDead: Boolean

  /**
    *
    * @return a safe copy of the life points
    */
  def lifePoints: Int

  /**
    * reset the life points
    */
  def resetLifePoints():Unit

  /**
    *
    * @return the spell launched
    */
  def spellToLaunch: Int

}

object Wizard {
  final val MaxLifePoints = 6

  def apply(name: String, id: Int): Wizard = WizardImpl(name, id)

  def unapply(arg: Wizard): Option[Wizard] = Some(arg)
}
