package it.unibo.ssb.model

trait WizardGameBoard extends Serializable {
  /**
    * The [[Wizard]] associated to this WizardGameBoard
    *
    * @return the current [[Wizard]]
    */
  def wizard: Wizard

  /**
    * Return just the ID of wizard
    * @return
    */
  def idWizard : Int

  /**
    *
    * @return a safe copy of the cards held by the wizard
    */
  def cardsList: Seq[Card]

  /**
    *
    * @param card to search
    *
    * @return true if the card is present
    */
  def containCard(card: Card): Boolean

  /**
    *
    * @param card to discard
    */
  def discard(card: Card):Unit

  /**
    *
    * @param card to add to the wizard board
    */
  def addCard(card: Card):Unit
}

object WizardGameBoard {
  def apply(wizard: Wizard): WizardGameBoard = WizardGameBoardImpl(wizard)
  def apply(id: Int):WizardGameBoard = WizardGameBoardImpl(Wizard("", id))
}
