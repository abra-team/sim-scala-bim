package it.unibo.ssb.model.remote

import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.Player
import it.unibo.ssb.model.Player.BasePlayer
import it.unibo.ssb.model.remote.Auth.UserPassAuth

import scala.concurrent.ExecutionContextExecutor

/** Abstract implementation of a player that also wraps an authentication token. */
trait AuthenticatedPlayer[T <: Auth] extends Player {

  /**
    * The authentication object that models a token to identify the player as himself/herself.
    *
    * @return the authentication token
    */
  def auth: T
}

case class SqlAuthenticatedPlayer(
    override val username: String,
    private val password: String,
    override val score: Int = 0,
    override val rankingPos: Int = 0,
    override val friends: Seq[Player] = Seq.empty
)(
    implicit private val webClient: WebClient,
    implicit private val executor: ExecutionContextExecutor
) extends BasePlayer(username) with AuthenticatedPlayer[UserPassAuth] {
  override val auth: SqlAuth = SqlAuth(username, password)
}

object SqlAuthenticatedPlayer {
  def apply(username: String, password: String)
    (implicit executor: ExecutionContextExecutor, webClient: WebClient): SqlAuthenticatedPlayer =
    new SqlAuthenticatedPlayer(username, password)
}