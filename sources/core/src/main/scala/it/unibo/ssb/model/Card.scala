package it.unibo.ssb.model

import java.util.ResourceBundle

import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.model.Spell._

sealed trait Card extends Serializable {
  def id: Int

  def name: String

  def description: String

  def spell: Spell
}

object Card extends LazyLogging {

  final val NumberOfSpells: Int = 8
  private final val IllegalCardReason: String = "Card ID should be an integer between 1 and 8"
  private val spellsBundle = ResourceBundle.getBundle("it/unibo/ssb/strings/spells")
  private val dragon = 1
  private val ghost = 2
  private val heal = 3
  private val owl = 4
  private val storm = 5
  private val wave = 6
  private val fire = 7
  private val potion = 8

  @throws[IllegalArgumentException](IllegalCardReason)
  def apply(id: Int): Card = {
    //noinspection ScalaStyle
    id match {
      case 1 => new Card_1
      case 2 => new Card_2
      case 3 => new Card_3
      case 4 => new Card_4
      case 5 => new Card_5
      case 6 => new Card_6
      case 7 => new Card_7
      case 8 => new Card_8
      case _ =>
        val e = new IllegalArgumentException(IllegalCardReason)
        logger.error(e.getLocalizedMessage, e)
        throw e
    }
  }

  @throws[IllegalArgumentException](IllegalCardReason)
  private def addNamesAndDescriptions(number: Int): (String, String) = number match {
    case this.dragon => (spellsBundle.getString("dragon_title"), spellsBundle.getString("dragon_effect"))
    case this.ghost=> (spellsBundle.getString("ghost_title"), spellsBundle.getString("ghost_effect"))
    case this.heal => (spellsBundle.getString("heal_title"), spellsBundle.getString("heal_effect"))
    case this.owl => (spellsBundle.getString("owl_title"), spellsBundle.getString("owl_effect"))
    case this.storm => (spellsBundle.getString("storm_title"), spellsBundle.getString("storm_effect"))
    case this.wave => (spellsBundle.getString("wave_title"), spellsBundle.getString("wave_effect"))
    case this.fire => (spellsBundle.getString("fire_title"), spellsBundle.getString("fire_effect"))
    case this.potion => (spellsBundle.getString("potion_title"), spellsBundle.getString("potion_effect"))
    case _ =>
      val e = new IllegalArgumentException(IllegalCardReason)
      logger.error(e.getLocalizedMessage, e)
      throw e
  }

  abstract case class AbstractCard(id: Int, name: String, description: String, spell: Spell) extends Card {

    override def equals(that: Any): Boolean = {
      that match {
        case that: Int => id.equals(that)
        case that: AbstractCard => id.equals(that.asInstanceOf[AbstractCard].id)
        case _ => super.equals(that)
      }
    }

    // Need to override for ScalaStyle best practice (when overriding equals() method
    override def hashCode(): Int = super.hashCode()

    override def canEqual(that: Any): Boolean = that.isInstanceOf[Card] || that.isInstanceOf[Int]
  }

  class Card_1() extends AbstractCard(this.dragon, addNamesAndDescriptions(this.dragon)._1,
    addNamesAndDescriptions(this.dragon)._2, Dragon())

  class Card_2() extends AbstractCard(this.ghost, addNamesAndDescriptions(this.ghost)._1,
    addNamesAndDescriptions(this.ghost)._2, Ghost())

  class Card_3() extends AbstractCard(this.heal, addNamesAndDescriptions(this.heal)._1,
    addNamesAndDescriptions(this.heal)._2, Forest())

  class Card_4() extends AbstractCard(this.owl, addNamesAndDescriptions(this.owl)._1,
    addNamesAndDescriptions(this.owl)._2, Owl())

  class Card_5() extends AbstractCard(this.storm, addNamesAndDescriptions(this.storm)._1,
    addNamesAndDescriptions(this.storm)._2, Storm())

  class Card_6() extends AbstractCard(this.wave, addNamesAndDescriptions(this.wave)._1,
    addNamesAndDescriptions(this.wave)._2, Wave())

  class Card_7() extends AbstractCard(this.fire, addNamesAndDescriptions(this.wave)._1,
    addNamesAndDescriptions(this.wave)._2, Fire())

  class Card_8() extends AbstractCard(this.potion, addNamesAndDescriptions(this.potion)._1,
    addNamesAndDescriptions(this.potion)._2, Potion())
}


