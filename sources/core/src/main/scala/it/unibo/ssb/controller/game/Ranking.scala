package it.unibo.ssb.controller.game

import it.unibo.ssb.model.Wizard

import scala.collection.mutable.ListBuffer

trait Ranking {

  /**
    * Method to be used for updating ranking
    *
    * @param wizard Wizard to update
    * @param score  Score gained or lose
    *
    * @throws java.lang.IllegalArgumentException if wizard not in list
    */
  @throws(classOf[IllegalArgumentException])
  def updateRanking(wizard: Wizard, score: Int): Unit

  /**
    * To get the current ranking
    *
    * @return sequence of wizard and their score
    */
  def ranking: Seq[(Wizard, Int)]

  /**
    * Method called to see if there are wizards that have hit the point needed to win the game
    * @return a list with wizard, his score, and if he is winner or not
    */
  def checkForWinner(): List[(Wizard, Boolean, Int)]

}

object Ranking {

  case class RankingImpl(wizards: Seq[Wizard], score: Int) extends Ranking {

    private val internalRanking = ListBuffer[(Wizard, Int)]() ++= wizards.map((_, 0))
    private val scoreToWin = score

    override def updateRanking(wizard: Wizard, score: Int): Unit = {
      if (!internalRanking.map(_._1).contains(wizard)) {
        throw new IllegalArgumentException("Wizard not in list")
      } else {
        val index = internalRanking.map(_._1).indexOf(wizard)
        val tmp_score = internalRanking(index)._2
        internalRanking.update(index, (wizard, tmp_score + score))
      }
    }

    override def ranking: Seq[(Wizard, Int)] = internalRanking.clone().seq

    override def checkForWinner() : List[(Wizard, Boolean, Int)]= internalRanking.filter(_._2>= scoreToWin)
      .map(w => (w._1, w._2 >= scoreToWin, w._2)).toList
  }

  /**
    *
    * @param wizards - the list of playing wizards
    * @param score the score to achieve for win
    * @return
    */
  def apply(wizards: Seq[Wizard], score: Int): Ranking = RankingImpl(wizards, score)

}


