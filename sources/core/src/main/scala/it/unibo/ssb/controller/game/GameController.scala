package it.unibo.ssb.controller.game

trait GameController extends GameControllerInitializer with GameControllerForPlay with DisconnectionHandling
  with Winnable

object GameController {

  /**
    * Create a gameController setting ranked false
    * @param rank game mode
    * @return
    */
  def apply(rank: Boolean = false): GameController = GameControllerSimple(rank)

  /**
    * Create a gameController setting both player and ranked
    * @param players players
    * @param rank game mode
    * @return
    */
  def apply(players: List[String], rank: Boolean): GameController = GameControllerSimple(players, rank)

}
