package it.unibo.ssb.controller

import it.unibo.ssb.controller.game.Dice
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TestDice extends FunSuite {
  test("A 6 faces dice must give me number in 1 6 range with the same frequency") {
    val vet: Array[Int] = Array(0, 0, 0, 0, 0, 0)
    for (_ <- 1 to 10000) {
      val tmp: Int = Dice.six().throwOnce()
      assert(tmp > 0 && tmp < 7)
      vet(tmp - 1) = vet(tmp - 1) + 1
    }
    vet.foreach(t => assert(t < 10000 / 6 + 200 && t > 10000 / 6 - 200))
  }
}