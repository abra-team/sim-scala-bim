package it.unibo.ssb.model

import org.scalatest.FunSuite

class cardTest extends FunSuite{
  test("Cards equal must work"){
    //noinspection ComparingUnrelatedTypes
    assert(Card(1).equals(1)) 
    assert(Card(1).equals(Card(1)))
  }
}
